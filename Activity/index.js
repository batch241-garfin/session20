
let number = Number(prompt("Enter a number larger than 50"));

for (let x = 0; x <= number; x++, number--)
{
	if (number <= 50)
	{
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}
	else if (number % 10 === 0)
	{
		console.log("The number is being skipped");
		continue;
	}
	else if (number % 5 === 0)
	{
		console.log(number);
	}
}

console.log("");


let superString = "supercalifragilisticexpialidocious";
let constString = "";

for(let x = 0; x < superString.length; x++)
{
	if (superString[x].toLowerCase() === "a" ||
		superString[x].toLowerCase() === "e" ||
		superString[x].toLowerCase() === "i" ||
		superString[x].toLowerCase() === "o" ||
		superString[x].toLowerCase() === "u"
		)
	{
		continue;
	}
	else
	{
		constString = constString + superString[x];
	}
}
console.log(constString);