
// Repetition Control Structures //


	// For Loop
	// While
	// Do While
	// Continue and Break

// While Loop //

// Condition first before statements

// Syntax:
// while (expression/condition)
// {
	// statement;
// }

let count = 5;

while (count !== 0)
{
	console.log("While: "+ count);
	count--;
}

// Do - while Loop

// Statements first before condition

// Syntax:
// do
// {
	// statement
// } while (expression/condition);

let number = Number(prompt("Give me a number"));

do
{
	console.log("Do-while: "+ number);
	number++;
} while (number < 10);

// For Loop //

// Consists of three parts. Initialization, Expression and Iteration

// Syntax:
// for(initial, condition, iterate)
// {
	// statement
// }

for (let counts = 0; counts <= 20; counts++)
{
	console.log(counts);
}

let myString = "Jeru";

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

for (let x = 0; x < myString.length; x++)
{
	console.log(myString[x]);
}

let myName = "JohnDaniel";

for (let i = 0; i < myName.length; i++)
{
	if (myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		)
	{
		console.log(3);
	}
	else
	{
		console.log(myName[i]);
	}
}

// Continue and Break //

// The continue statement allows the code to go to next iteration of
// the loop without finishing the execution of a statements in
// a code block.

for (let count1 = 0; count1 <= 20; count1++)
{
	if (count1 % 2 === 0)
	{
		continue;
	}
	console.log("Continue and Break: "+ count1);

	if (count1 > 10)
	{
		break;
	}
}

let name = "jakelexter";

for (let i = 0; i < name.length; i++)
{
	console.log(name[i]);

	if (name[i].toLowerCase() === "a")
	{
		console.log("Continue to the next iteration");
		continue;
	}

	if (name[i] === "x")
	{
		break;
	}
}